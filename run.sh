if [ -f "/usr/local/bin/python3.4" ]; then
	echo "Python installed"
else 
	sudo yum groupinstall -y development
	sudo yum install -y zlib-dev openssl-devel sqlite-devel bzip2-devel
	sudo wget http://www.python.org/ftp/python/3.4.3/Python-3.4.3.tar.xz
	sudo xz -d Python-3.4.3.tar.xz
	sudo tar -xvf Python-3.4.3.tar
	cd ./Python-3.4.3
	sudo ./configure
	sudo make
	sudo make install
	source /home/vagrant/.bash_profile
	source /home/vagrant/.bashrc
	which python3.4
	sudo cd /home/vagrant/
	rm -rf /home/vagrant/Python-3.4.*
fi

python_path=/usr/local/bin/

if which mysql; then
	echo "MySQL is already installed"
else
	rpm -iUvh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
	sudo yum -y install mysql
	sudo yum -y install mysql-devel
	sudo yum -y install mysql-server
	sudo service mysql start
fi
mysql -u root -e "drop database madnetsu;"
mysql -u root -e "create database madnetsu CHARACTER SET utf8 COLLATE utf8_general_ci;"

if which psql; then
	echo "Postgres installed already"
else
	rpm -iUvh http://yum.postgresql.org/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-1.noarch.rpm
	yum -y update
	yum -y install postgresql94 postgresql94-server postgresql94-contrib postgresql94-libs postgresql94-devel
	sudo /usr/pgsql-9.4/bin/postgresql94-setup initdb
	sudo chkconfig postgresql-9.4 on
fi
export PATH=$PATH:/usr/pgsql-9.4/bin/:/usr/local/bin/
sudo -u root sh
export PATH=$PATH:/usr/pgsql-9.4/bin/:/usr/local/bin/
echo $PATH
/usr/local/bin/pip3.4 install --upgrade pip
/usr/local/bin/pip3.4 install -r /vagrant/requirements.txt
cd ~ 
service postgresql-9.4 start
sudo -u postgres createuser ngavrish
sudo -u postgres createdb ngavrish
sudo -u postgres createuser vagrant
sudo -u postgres createdb vagrant

sudo -u vagrant bash
echo "WHOAMI"
whoami
cd /home/vagrant/
if [ -d "/home/vagrant/nuclear-case/" ]; then
	rm -rf "/home/vagrant/nuclear-case/"
fi
if [ -d "/home/vagrant/madnet/" ]; then
	rm -rf "/home/vagrant/madnet"
fi 
git clone https://ngavrish@bitbucket.org/ngavrish/nuclear-case.git
git clone https://ngavrish@bitbucket.org/ngavrish/madnet.git

cd /home/vagrant/madnet/madnetsu/madnetsu
pwd
/usr/local/bin/python3.4 manage.py syncdb
/usr/local/bin/python3.4 manage.py makemigrations
/usr/local/bin/python3.4 manage.py migrate
echo "Migration success"
mysql -u root madnetsu < mysqlroutines
systemctl stop firewalld
systemctl disable firewalld.service
#sudo /usr/local/bin/python3.4 ./manage.py runserver 0.0.0.0:8000
