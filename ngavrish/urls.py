from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from rest_framework import routers
from app import views

admin.autodiscover()

# TODO: find out what the hell this means
class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]

    def get(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            user = authenticate()
            login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'article', views.ArticleViewSet)
router.register(r'category', views.CategoryViewSet)
router.register(r'projects', views.ProjectViewSet)
router.register(r'products', views.ProductViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('', include('social.apps.django_app.urls', namespace='social')),

    url(r'^projects.html', TemplateView.as_view(template_name='app/pages/projects.html')),
    url(r'^home.html', TemplateView.as_view(template_name='app/pages/home.html')),
    url(r'^cookbook.html', TemplateView.as_view(template_name='app/pages/cookbook.html')),
    url(r'^article.html', TemplateView.as_view(template_name='app/pages/article.html')),
    url(r'^whatis.html', TemplateView.as_view(template_name='app/pages/whatis.html')),
    # url(r'^buildserver.html', TemplateView.as_view(template_name='app/pages/buildserver.html')),
    # url(r'^missiles.html', TemplateView.as_view(template_name='app/pages/missiles.html')),
    # url(r'^missiles.html', TemplateView.as_view(template_name='app/pages/directives/missiles.html')),
    url(r'^directives/projectSummary.html', TemplateView.as_view(template_name='app/directives/projectSummary.html')),
    url(r'^directives/buildProjectSummary.html', TemplateView.as_view(template_name='app/directives/buildProjectSummary.html')),
    url(r'^directives/cookbookBar.html', TemplateView.as_view(template_name='app/directives/cookbookBar.html')),
    url(r'^directives/cookbookBarNoCategories.html', TemplateView.as_view(template_name='app/directives/cookbookBarNoCategories.html')),

    # url(r'^$', views.index, name='index'),
    url(r'^logout/$', views.logout_handler, name='logout'),

    url(r'^$', views.index, name='index'),
    # url(r'^projects$', TemplateView.as_view(template_name='app/index.html')),
        
)
