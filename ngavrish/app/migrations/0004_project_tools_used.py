# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20150505_2121'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='tools_used',
            field=models.CharField(max_length=256, default='Java'),
            preserve_default=False,
        ),
    ]
