# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='product',
            field=models.ForeignKey(default=1, to='app.Product'),
            preserve_default=False,
        ),
    ]
