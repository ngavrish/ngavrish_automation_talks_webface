# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_category_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='next_name',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='article',
            name='prev_name',
            field=models.TextField(default=''),
        ),
    ]
