# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20150507_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='tools',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
