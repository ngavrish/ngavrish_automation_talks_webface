# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_article_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleTranslation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('introduction', models.TextField()),
                ('content', models.TextField()),
                ('conclusion', models.TextField()),
                ('language_code', models.CharField(db_index=True, max_length=15)),
            ],
            options={
                'db_tablespace': '',
                'db_table': 'app_article_translation',
                'managed': True,
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='article',
            name='conclusion',
        ),
        migrations.RemoveField(
            model_name='article',
            name='content',
        ),
        migrations.RemoveField(
            model_name='article',
            name='introduction',
        ),
        migrations.AddField(
            model_name='articletranslation',
            name='master',
            field=models.ForeignKey(related_name='translations', null=True, to='app.Article', editable=False),
        ),
        migrations.AlterUniqueTogether(
            name='articletranslation',
            unique_together=set([('language_code', 'master')]),
        ),
    ]
