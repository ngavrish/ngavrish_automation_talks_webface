# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_article_order_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='ended',
            field=models.DateField(blank=True),
        ),
    ]
