# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20150507_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='article_name',
            field=models.TextField(),
        ),
    ]
