# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_remove_project_tools'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='role',
        ),
        migrations.AddField(
            model_name='article',
            name='created',
            field=models.DateField(default=datetime.datetime(2015, 5, 7, 16, 52, 51, 767850, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
