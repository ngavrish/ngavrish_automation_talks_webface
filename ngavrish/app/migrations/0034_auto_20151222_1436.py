# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sessions', '0001_initial'),
        ('app', '0033_auto_20151221_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='youtube_url',
            field=models.URLField(blank=True, null=True, default=None),
        ),
    ]
