# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_project_tools_used'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='tools',
        ),
        migrations.AddField(
            model_name='project',
            name='conclusion',
            field=models.TextField(default='All was well'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='project',
            name='team',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='project',
            name='tools_used',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
