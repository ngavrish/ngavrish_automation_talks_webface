# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20150625_1740'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='order_number',
            field=models.IntegerField(default=1),
        ),
    ]
