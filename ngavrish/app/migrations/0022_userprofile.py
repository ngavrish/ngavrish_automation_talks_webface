# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0021_auto_20150729_1108'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('email', models.EmailField(max_length=254)),
                ('username', models.TextField()),
                ('gender', models.CharField(blank=True, max_length=20, null=True, choices=[('male', 'Male'), ('female', 'Female')])),
                ('city', models.CharField(blank=True, max_length=250, null=True)),
                ('dob', models.DateField(blank=True, null=True)),
                ('locale', models.CharField(blank=True, max_length=10, null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
