# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('article_name', models.CharField(max_length=256)),
                ('introduction', models.TextField()),
                ('content', models.TextField()),
                ('conclusion', models.TextField()),
                ('role', models.CharField(max_length=256)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('template', models.CharField(max_length=256)),
                ('content', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('project_name', models.CharField(max_length=256)),
                ('description', models.TextField()),
                ('url', models.CharField(max_length=256)),
                ('team', models.CharField(max_length=256)),
                ('role', models.CharField(max_length=256)),
                ('tools', models.CharField(max_length=256)),
                ('started', models.DateField()),
                ('ended', models.DateField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
