# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0031_auto_20150924_1146'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='user_ptr',
        ),
    ]
