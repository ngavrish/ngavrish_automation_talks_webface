# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20150505_2121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='url',
            field=models.CharField(max_length=256),
            preserve_default=True,
        ),
    ]
