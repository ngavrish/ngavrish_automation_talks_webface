# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20150924_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='date_joined',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 24, 11, 39, 52, 521224, tzinfo=utc)),
        ),
        migrations.AlterModelTable(
            name='userprofile',
            table='app_userprofile',
        ),
    ]
