# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20150625_0714'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='conclusion',
            field=models.CharField(max_length=256),
        ),
    ]
