// CONTROLLERS
ngavrish.controller('projectsController', ['$scope', '$http', function ($scope, $http) {
    console.log("Projects Controller");
//    ====
    $http.get('/api/projects/').success(function (data, status, headers, config) {
        $scope.projects_list = data.results.sort(function (a, b) {
            return parseFloat(b.id) - parseFloat(a.id);
        });
        console.log('Projects = ' + JSON.stringify($scope.projects_list));
    }).error(function (data, status, headers, config) {
            console.log(data);
        });
}]);

//ngavrish.controller('blogController', ['$scope', '$http', '$timeout', '$location', 'sourceTypeService',
//                    function($scope, $http, $timeout, $location, sourceTypeService) {
//    var host = $location.protocol() + "://" + $location.host() +':'+  $location.port();
//    $scope.errorMessage = "";
//    $scope.zipfile = null;
//    $scope.source_type = sourceTypeService.source_type;
//    console.log("Build Server Controller");
//
//    $scope.toggle_new_project_panel = function() {
//        $("#new_project_form").toggle()
//    };
//    $scope.isGit = function(source_type) {
//        return source_type === 'git'
//    };
//    $scope.isZip = function(source_type) {
//        return source_type === 'zip'
//    };
//
//    $http.get('/api/blog/').success(function(data, status, headers, config) {
//        $scope.build_projects = data.results.sort(function(a, b) {
//            return parseFloat(a.pk) - parseFloat(b.pk);
//        });
//    }).error(function(data, status, headers, config) {
//        console.log(data);
//    });
//
//    $scope.create_project = function() {
//        console.log('Create object');
//        if ($scope.source_type) {
////            source type is not empty
//            if ($('.source-error-message').hasClass('hidden') !== true) {
//                $('.source-error-message').addClass('hidden');
//            }
//        } else {
////            source type is empty
//            $('.source-error-message').removeClass('hidden');
//            $scope.errorMessage = "Choose source type!";
//            $timeout(function() {
//                $scope.$apply();
//            });
//        }
//
////        TODO:
////        get user from server by session_id
//        var data = {user: 2,
//            email: $("#notificationEmail1").val(),
//            name: $("#projectname").val(),
//            git_username: $("#gitUsername").val(),
//            git_password: $("#gitPassword").val(),
//            git_clone_url: $("#gitUrl").val(),
//            test_root_path: $("#testsRoot").val(),
//            build_script_path: $("#buildScript").val()};
//
//        var fd = new FormData();
//        fd.append('file', $scope.zipfile);
//        fd.append('data', angular.toJson(data));
//
//        $http.post('/api/buildprojects/', fd, {
//                  transformRequest: angular.identity,
//                  headers: {
//                    'Content-Type': undefined
//                  }
//                })
//            .success(function (data, status, headers, config) {
//                console.log("buildprojects post success " + data);
//                $("#new_project_form").css('display', 'none');
//                $("#projectcreated_success").show().delay(1000).animate({
//                                                                opacity: 0,
//                                                                height: "toggle"
//                                                              }, 2500, function() {
//                                                                // Animation complete.
//                                                              });
//                $http.get('/api/buildprojects/').success(function(data, status, headers, config) {
//                    $scope.build_projects = data.results.sort(function(a, b) {
//                        return parseFloat(a.pk) - parseFloat(b.pk);
//                    });
//                }).error(function(data, status, headers, config) {
//                    console.log(data);
//                });
//                $scope.$apply()
//            }).error(function (data, status, headers, config) {
//                var status = status;
//                var header = headers;
//                console.log("buildprojects post error " + data);
//            });
//    };
//
//    $("div[aria-label='source-type'] > div > button").click(function() {
//        $("div[aria-label='source-type'] > div > button").removeClass('active');
//        $(this).addClass('active');
//        $scope.source_type = $(this).attr('source-type');
//        $('div.form-group[source-type="' + $scope.source_type + '"]').removeClass('hidden');
//        $('div.form-group[source-type][source-type!="' + $scope.source_type + '"]').addClass('hidden');
//        console.log("Source type service = " + $scope.source_type);
//        $timeout(function() {
//            $scope.$apply();
//        });
//    });
//
//
//    $('.footer').attr('style', 'margin: 0 0 0 0; bottom: 0;');
//
//}]);

ngavrish.controller('whatisController', ['$scope', '$http', function ($scope, $http) {
    console.log("Whatis controller");
    var offset = 0;
    $scope.scrollto = function ($event) {
        var target = $("#" + $event.target.target);
        console.log(target.offset().top);
        offset = offset + target.offset().top;
        $("div[whatis] ul > li").removeClass("active");
        $($event.target).parent().addClass('active');
        $('body, html, .parallax').animate({scrollTop: offset}, 1000);
    }

}]);

ngavrish.controller('cookbookController', ['$scope', '$http', '$location', '$anchorScroll',
    function ($scope, $http, $location, $anchorScroll) {
        $("#footer").css("position", "relative");
        $("div.row.menu").css("top", $("#articles").css("top"));
        console.log('Cookbook controller');
        var locale = 'en';
        $scope.searchText = '';

        $http.get('/api/category/', {params: {product: "Automation Cookbook"}}).success(function (data, status, headers, config) {
            $scope.cookbook_categories_list = data.sort(function (a, b) {
                return parseFloat(a.order_number) - parseFloat(b.order_number);
            });
            return data;
        }).then(function (data) {
                $http.get('/api/article/', {params: {lang: locale}}).success(function (data, status, headers, config) {
                    $scope.linear_talks = data.sort(function (a, b) {
                        return parseFloat(a.order_number) - parseFloat(b.order_number);
                    });
                    return data;
                }).then(function () {
                        $scope.linear_talks.forEach(function (talk) {
                            talk.article_name = talk.article_name.replace(" ", "_");
                            var category_id = talk.category.split('/').slice(-2, -1)[0];
                            $scope.cookbook_categories_list.forEach(function (cat) {
                                if (cat.id === parseInt(category_id)) {
                                    if (cat.talks === undefined) {
                                        cat.talks = []
                                    }
                                    cat.talks.push(talk);
                                    console.log(JSON.stringify(cat));
                                }
                            })
                        });

                        var sublist = [];
                        $scope.cookbook_categories_list.forEach(function (category) {
                            if (category.talks !== undefined) {
                                for (var i = 0; i < category.talks.length; i++) {
                                    sublist.push(category.talks[i]);
                                    if ((i + 1) % 3 === 0) {
                                        if (category.talk_rows === undefined) {
                                            category.talk_rows = []
                                        }
                                        category.talk_rows.push(sublist);
                                        sublist = [];
                                    }
                                }
                                if (sublist.length !== 0) {
                                    if (category.talk_rows === undefined) {
                                        category.talk_rows = [];
                                    }
                                    category.talk_rows.push(sublist);
                                    sublist = [];
                                }
                            }
                        });
                        $scope.categories_list_tableview = $scope.cookbook_categories_list;
                        console.log('Talks = ' + JSON.stringify($scope.talks_list));
                    });
            });

        $scope.scroll2Anchor = function (hash) {
            $location.hash(hash);
            $anchorScroll();
        };

        $scope.linearFilter = function (actual, expected) {
            return actual.article_name.indexOf(expected) >= 0;
        };
        $scope.changeListView = function ($event) {
            var bars = $('.talkBars'),
                links = $('.talkLinks');
            if ($($event.target).hasClass('cuts')) {
                if ($(bars).css('display') === 'none') {
                    $(bars).css('display', 'block');
                    $(links).css('display', 'none');
                    $('div.list').removeClass('active');
                    $('div.cuts').addClass('active');
                }
            }
            if ($($event.target).hasClass('list')) {
                if ($(links).css('display') === 'none') {
                    $(links).css('display', 'block');
                    $(bars).css('display', 'none');
                    $('div.cuts').removeClass('active');
                    $('div.list').addClass('active');
                }
            }
        };

    }]);

ngavrish.controller('articleController', ['$scope', '$http', '$routeParams', '$sce', '$location', '$timeout',
    function ($scope, $http, $routeParams, $sce, $location, $timeout) {
        var locale = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en';
        console.log('Article controller. Read article #' + $routeParams.name);
        $scope.location = $location.absUrl();

        $http.get('/api/article/', {params: {lang: locale}}).success(function (data, status, headers, config) {
            $scope.talks_amount = data.length
        }).then(function () {

                $http.get('/api/article/' + $routeParams.name + "/", {params: {lang: locale}}).success(function (data, status, headers, config) {
                    // Load the IFrame Player API code asynchronously.

                    $scope.talk = data;
                    var video_id = $scope.talk.youtube_url.split("=")[1];
                    if (video_id) {
                        $scope.url = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + video_id);
                    }
                    $scope.talk.introduction = $sce.trustAsHtml($scope.talk.introduction);
                    $scope.talk.content = $sce.trustAsHtml($scope.talk.content);
                    $scope.talk.conclusion = $sce.trustAsHtml($scope.talk.conclusion);
                });
            });

    }]);