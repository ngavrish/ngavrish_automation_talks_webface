// DIRECTIVES
ngavrish.directive("projectSummary", function() {
   return {
       restrict: 'E',
       templateUrl: 'directives/projectSummary.html',
       replace: true,
       scope: {
           project: "="
       }
   }
});

ngavrish.directive("buildProjectSummary", function() {
   return {
       restrict: 'E',
       templateUrl: 'directives/buildProjectSummary.html',
       replace: true,
       scope: {
           project: "="
       },
       controller: function($scope, $element, $http, $location, $anchorScroll) {

           $scope.toggle_existing_project_panel = function() {
               $("#update_project_" + $scope.project.pk).toggle();
           };
           $scope.delete_project = function() {
                $http.delete('/api/buildprojects/' + $scope.project.pk)
                   .success(function (data, status, headers, config) {
                    console.log("buildprojects delete success " + data);
                    $("div[ng-buildproject='buildproject_" + $scope.project.pk + "']").css('display', 'none');
                    $http.get('/api/buildprojects/').success(function(data, status, headers, config) {
                        $scope.$parent.build_projects = data.results.sort(function(a, b) {
                            return parseFloat(a.pk) - parseFloat(b.pk);
                        });
                    }).error(function(data, status, headers, config) {
                        console.log(data);
                    });
                    $scope.$parent.$apply()
                }).error(function (data, status, headers, config) {
                    var status = status;
                    var header = headers;
                    console.log("buildprojects post error " + data);
                });
           };

           $scope.update_project = function() {
                var data = {user: 2,
                            email: $("#update_project_" + $scope.project.pk + " #notificationEmail1").val(),
                            name: $("#update_project_" + $scope.project.pk + " #projectname").val(),
//                            git_username: $("#update_project_" + $scope.project.pk + " #gitUsername").val(),
//                            git_password: $("#update_project_" + $scope.project.pk + " #gitPassword").val(),
//                            git_clone_url: $("#update_project_" + $scope.project.pk + " #gitUrl").val(),
                            test_root_path: $("#update_project_" + $scope.project.pk + " #testsRoot").val(),
                            build_script_path: $("#update_project_" + $scope.project.pk + " #buildScript").val()};

                var fd = new FormData();
                fd.append('file', $scope.zipfile);
                fd.append('data', angular.toJson(data));
                var url = '/api/buildprojects/' + $scope.project.pk + "/";
                $http.put(url, fd, {
                      transformRequest: angular.identity,
                      headers: {
                        'Content-Type': undefined
                      }
                   })
                   .success(function (data, status, headers, config) {
                    console.log("buildprojects delete success " + data);
                    $("div[ng-buildproject='buildproject_" + $scope.project.pk + "']").css('display', 'none');
                    $http.get('/api/buildprojects/').success(function(data, status, headers, config) {
                        $scope.$parent.build_projects = data.results.sort(function(a, b) {
                            return parseFloat(a.pk) - parseFloat(b.pk);
                        });
                    }).error(function(data, status, headers, config) {
                        console.log(data);
                    });
//                    $scope.$parent.$apply()
                }).error(function (data, status, headers, config) {
                    var status = status;
                    var header = headers;
                    console.log("buildprojects post error " + data);
                });
           };
       }
   }
});

ngavrish.directive("cookbookBar", function() {
   return {
       restrict: 'EA',
       templateUrl: 'directives/cookbookBar.html',
       replace: true,
       scope: {
           row: "=",
           searchQuery: "="
       },
       controller: function($scope, $element, $http, $sce) {
           $scope.row.forEach(function(talk) {
              talk.introduction = $sce.trustAsHtml(talk.introduction );
           });
       }
   }
});

ngavrish.directive("cookbookBarNoCategories", function() {
   return {
       restrict: 'EA',
       templateUrl: 'directives/cookbookBarNoCategories.html',
       replace: true,
       scope: {
           recipe: "="
       }
   }
});

ngavrish.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'EA',
        bindToController: true,
        transclude: true,
        link: function($scope, $element, $attrs) {
            var model = $parse($attrs.fileModel);
            var modelSetter = model.assign;

            $element.bind('change', function() {
                $scope.$parent.zipfile = $element[0].files[0];
                $scope.$apply();
            });
        }
    };
}]);

//ngavrish.directive("projectSummary", function() {
//   return {
//       restrict: 'E',
//       templateUrl: 'directives/projectSummary.html',
//       replace: true,
//       scope: {
//           project: "=",
//           cities: "=",
//           convertToStandard: "&",
//           convertToDate: "&",
//           dateFormat: "@"
//       },
//       controller: function($scope, $element, $http) {
//            $scope.retarget = function(missile, city) {
//                console.log("Retarget on " + city.fields.name);
//                $http.post('/retarget/' + missile.pk, {target_name:city.fields.name}).
//                  success(function(data, status, headers, config) {
//                    console.log("Success");
//                    $scope.missile.fields.target_city=city.pk
//                    console.log("Scope: ");
//                    console.log($scope);
//                    console.log($element);
////                    $scope.$apply();
//                    // this callback will be called asynchronously
//                    // when the response is available
//                  }).
//                  error(function(data, status, headers, config) {
//                     console.log("error");
//                    // called asynchronously if an error occurs
//                    // or server returns response with an error status.
//                  });
//            };
//            $scope.city_is_target = function(missile, city) {
//                if (city.pk == missile.fields.target_city) {
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        }
//   }
//});