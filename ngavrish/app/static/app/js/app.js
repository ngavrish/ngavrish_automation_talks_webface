//HOST = "http://qalalab.ru";
LOCALES = {EN: 'en', RU: 'ru'};

$(window).on('hashchange', function(e){
    nav_bar_activation();
    shiftFooter();
});

function get_controller() {
    if (window.location.hash.split('/').length >= 2) {
        return window.location.hash.split('/')[1];
    } else {
        return "whatis";
    }
}

function nav_bar_activation() {
    var controller = get_controller();
    if (controller == "") {
        controller = "whatis"
    }

    $('ul.nav.navbar-nav li').removeClass('active');
    $('ul.nav.navbar-nav li[controller=' + controller + ']').addClass('active');
    console.log(localStorage.getItem('locale'));
    if ((localStorage.getItem('locale') === null) || (localStorage.getItem('locale') === LOCALES.EN)) {
        $('.nav.lang > li').removeClass('lang_active');
        $('.nav.lang > li.en').addClass('lang_active')
    } else if (localStorage.getItem('locale') === LOCALES.RU) {
        $('.nav.lang > li').removeClass('lang_active');
        $('.nav.lang > li.ru').addClass('lang_active')
    }
}

$(document).ready(function () {
    nav_bar_activation();
    fb_init();
    $('.nav.lang > li.en').click(function() {
        localStorage.setItem("locale" , LOCALES.EN);
        nav_bar_activation();
        location.reload();
    });
    $('.nav.lang > li.ru').click(function() {
        localStorage.setItem("locale" , LOCALES.RU);
        nav_bar_activation();
        location.reload();
    });
});

$(window).load(function() {
   $("main").css("top", $("nav").height());
});

// MODULE
var ngavrish = angular.module('ngavrish', ['ngRoute', 'ngResource', 'ngSanitize'], function ($httpProvider) {
//    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.headers.post['Content-Type'] = false;
    var param = function (obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for (name in obj) {
            value = obj[name];

            if (value instanceof Array) {
                for (i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
});

function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('fb_login_status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('fb_login_status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

var shiftFooter = function () {
    if (window.location.hash.indexOf("blog") > -1) {
        waitForElement(".talksWrapper", function () {
            var footer_top = $("#footer").position().top,
                content_bottom = $(".container").height() + $(".container").position().top,
                talks_wrapper = $(".talksWrapper").height() + $(".talksWrapper").position().top;
            if (talks_wrapper > footer_top) {
                $("#footer").css("position", "relative");
            } else {
                $("#footer").css("position", "absolute");
                $("#footer").css("bottom", "-10px");
            }
        });
    } else {
        $("#footer").css("position", "relative");
    }
};

function fb_init() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1531020270549560',
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.5' // use version 2.2
      });
      FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
      });
      FB.Event.subscribe('xfbml.render', function(response) {
          shiftFooter()
      });
    };

    (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}


var waitForElement = function(elementPath, callBack){
  window.setTimeout(function(){
    if($(elementPath).length){
      callBack(elementPath, $(elementPath));
    }else{
      waitForElement(elementPath, callBack);
    }
  },500)
};

//function testAPI() {
//    console.log('Welcome!  Fetching your information.... ');
//    FB.api('/me', function(response) {
//      console.log('Successful login for: ' + response.name);
//
//      $("#fb_login_button").hide();
//    });
//}