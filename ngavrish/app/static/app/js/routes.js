// ROUTES
ngavrish.config(function ($routeProvider) {
   
    $routeProvider
    .when('/', {
        templateUrl: '/whatis.html',
        controller: 'whatisController'
    })
    .when('/projects', {
        templateUrl: '/projects.html',
        controller: 'projectsController'
    })
    .when('/blog', {
        templateUrl: '/cookbook.html',
        controller: 'cookbookController'
    })
    .when('/blog/article/:name', {
        templateUrl: '/article.html',
        controller: 'articleController'
    })
    .when('/whatis', {
        templateUrl: '/whatis.html',
        controller: 'whatisController'
    })
//    .when('/', {
//        templateUrl: '/home.html',
//        controller: 'homeController'
//    })

//    .when('/ui/missles/:days', {
//        templateUrl: '/pages/missles.htm',
//        controller: 'forecastController'
//    })
    
});