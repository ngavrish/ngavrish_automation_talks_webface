from django.contrib.auth import logout
from django.contrib.auth.models import User, Group
from django.http import Http404
from django.shortcuts import render_to_response, get_object_or_404, redirect, render
# Create your views here.
from rest_framework import viewsets
from app.serializers import UserSerializer, GroupSerializer
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from app.models import Article, Category, Project, Product
from app.serializers import ArticleSerializer, ProjectSerializer, CategorySerializer, ProductSerializer
from rest_framework.response import Response


def logout_handler(request):
    logout(request)
    return redirect('/#/')


def index(request):
    return render(request, 'app/index.html', locals())


@permission_classes((IsAuthenticated, ))
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


@permission_classes((IsAuthenticated, ))
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


@permission_classes((AllowAny, ))
class ArticleViewSet(viewsets.GenericViewSet):
    """
    API endpoint that allows articles to be viewed or edited.
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request):
        latest_amount = int(request.GET.get("latest_amount", 0))
        if not latest_amount:
            queryset = Article.objects.language(language_code=request.GET.get("lang", "ru")).all()
            serializer = ArticleSerializer(queryset, many=True, context={'request': request})
            return Response(serializer.data)
        else:
            queryset = Article.objects.language(language_code=request.GET.get("lang", "ru")).all().order_by('-pk')[:latest_amount]
            serializer = ArticleSerializer(queryset, many=True, context={'request': request})
            return Response(serializer.data)

    def retrieve(self, request, pk):
        queryset = Article.objects.language(language_code=request.GET.get("lang", "ru")).all()
        pk = pk.replace("_", " ")
        article = get_object_or_404(queryset, article_name=pk)
        serializer = ArticleSerializer(article, context={'request': request})
        if type(article) is not Http404:
            if article.order_number < len(queryset):
                article.next_name = Article.objects.get(order_number=article.order_number+1).__dict__.get("article_name","").replace(" ", "_")
            if article.order_number > 1:
                article.prev_name = Article.objects.get(order_number=article.order_number-1).__dict__.get("article_name","").replace(" ", "_")
        return Response(serializer.data)


# @permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
class ProjectViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


@permission_classes((AllowAny, ))
class CategoryViewSet(viewsets.GenericViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def list(self, request):
        queryset = Category.objects.filter(product=Product.objects.get(product_name=request.GET.get("product", "Automation Cookbook")))
        serializer = CategorySerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    def retrieve(self, request, pk):
        queryset = Category.objects.all()
        category = get_object_or_404(queryset, order_number=pk)
        serializer = CategorySerializer(category, context={'request': request})
        return Response(serializer.data)


# @permission_classes((IsAuthenticated, ))
class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows projects to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


# ============= UTILS ============

def get_object_or_none(queryresult):
    if len(queryresult) != 1:        
        return None
    return queryresult[0]