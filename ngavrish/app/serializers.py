from app.models import Article, Product
from app.models import Category
from app.models import Project
from hvad.contrib.restframework import HyperlinkedTranslatableModelSerializer
__author__ = '802619'
# Serializers define the API representation.
from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'order_number', 'cat_name', 'product')


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'product_name')


class ArticleSerializer(HyperlinkedTranslatableModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'order_number', 'introduction', 'article_name', 'content', 'conclusion', 'created', 'category',
                  'next_name', 'prev_name', 'youtube_url')


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'project_name', 'description', 'url', 'tools_used', 'team', 'role', 'started', 'ended', 'conclusion')

# ViewSets define the view behavior.
# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
