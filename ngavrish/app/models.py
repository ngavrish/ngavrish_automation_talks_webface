'''
Created on Feb 6, 2015

@author: Gavrish
'''
from django.contrib.auth.models import User
from django.db import models
import math
from django.contrib.auth.signals import user_logged_in
from hvad.models import TranslatableModel, TranslatedFields
from django.contrib.sessions.models import Session


class Content(models.Model):
    template = models.CharField(max_length=256)
    content = models.TextField()

    def __str__(self):
        return str(self.id) + '_' + self.template

    def __hash__(self):
        return self.id + math.pow(self.id, self.id)


class Project(models.Model):
    project_name = models.CharField(max_length=256)
    description = models.TextField()
    url = models.CharField(max_length=256)
    team = models.TextField()
    role = models.CharField(max_length=256)
    started = models.DateField()
    ended = models.DateField(blank=True)
    tools_used = models.TextField()
    conclusion = models.TextField()

    def __str__(self):
        return str(self.id) + '_' + self.project_name

    def __hash__(self):
        return int(self.id + math.pow(self.id, self.id))


class Product(models.Model):
    product_name = models.CharField(max_length=256, default="Automation Cookbook")

    def __str__(self):
        return str(self.id) + '_' + self.product_name

    def __hash__(self):
        return int(self.id + math.pow(self.id, self.id))


class Category(models.Model):
    cat_name = models.CharField(max_length=256)
    product = models.ForeignKey(Product)
    order_number = models.IntegerField(default=1)

    def __str__(self):
        return str(self.id) + '_' + self.cat_name

    def __hash__(self):
        return int(self.id + math.pow(self.id, self.id))


class Article(TranslatableModel):
    order_number = models.IntegerField(default=1)
    article_name = models.TextField()
    prev_name = models.TextField(default="")
    next_name = models.TextField(default="")

    translations = TranslatedFields(
        introduction=models.TextField(),
        content=models.TextField(),
        conclusion=models.TextField())

    created = models.DateField()
    category = models.ForeignKey(Category)
    youtube_url = models.URLField(default=None, blank=True, null=True)

    def __str__(self):
        return str(self.id) + '_' + self.article_name

    def __hash__(self):
        return int(self.id + math.pow(self.id, self.id))


class UserSession(models.Model):
    user = models.ForeignKey(User)
    session = models.ForeignKey(Session)

    def user_logged_in_handler(sender, request, user, **kwargs):
        if not user.is_superuser:
            UserSession.objects.get_or_create(user=user, session_id=request.session.session_key)

    user_logged_in.connect(user_logged_in_handler)