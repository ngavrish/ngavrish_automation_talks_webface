from django.contrib import admin

# Register your models here.
from app.models import Content, Article, Project, Category
from hvad.admin import TranslatableAdmin
from app.models import Product

admin.site.register(Content)
admin.site.register(Article, TranslatableAdmin)
admin.site.register(Project)
admin.site.register(Category)
admin.site.register(Product)